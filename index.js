
const FIRST_NAME = "IOANA";
const LAST_NAME = "nagit";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function numberParser(a)
{
  if ( a === NaN || Infinity >= a >= -Infinity || Number.MIN_INTEGER_VALUE > a > Number.MAX_INTEGER_VALUE)
  {
      if(a === Infinity || a === -Infinity || a > Number.MAX_INTEGER_VALUE || a < Number.MIN_INTEGER_VALUE)
      {
          return NaN;
      }
      return Math.round(a);
  }
  else return NaN;

        
}


function dynamicPropertyChecker(input, property) {
    
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

console.log(numberParser(56.7));
console.log(parseInt('56.7'));
console.log(numberParser(Infinity));